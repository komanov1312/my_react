import React from 'react';
import Footer from './pages/Footer';
import Header from './pages/Header';
import Content from './pages/Content'

function Layout() {
  return (
    <>
    <Header
      title="номер 1"
      htmlClass=" l-header--mod" />
    <Content />
    <Footer />
    </>
  );
}

export default Layout;
